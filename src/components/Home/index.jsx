import React, { useEffect } from 'react';
import axiosInstance from  '../../utilis/axiosInstance';
import './home.css';
const Home = () => {
  useEffect(() => {
    // Example API call
    axiosInstance.get('https://jsonplaceholder.typicode.com/todos/1')
      .then(response => {
        console.log(response.data);
      })
      .catch(error => {
        console.error(error);
      });

      configureClient();
  }, []);
  let auth0Client = null;
  const fetchAuthConfig = () => fetch('/auth_config');


const configureClient = async () => {
  //  const response = await fetchAuthConfig();
   // const config = await response.json();

    // auth0Client = await auth0.createAuth0Client({
    //     domain: config.domain,
    //     clientId: config.clientId,
    // });
};

const logout = () => {
    console.log('logout')
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');

    // auth0Client.logout({
    //     logoutParams: {
    //         returnTo: window.location.origin,
    //     },
    // });
};

const login = async () => {
    console.log('login')
    // await auth0Client.loginWithRedirect({
    //     authorizationParams: {
    //         redirect_uri: window.location.origin,
    //     },
    // });
};
  return (
    <div>
    <button id="btn-login" className="signin"  onClick={login}>Log in</button>
    <button id="btn-logout" className="signout"  onClick={logout}>Log out</button>

    <h1 className="text-3xl font-bold hover:underline">
      Hello world!
    </h1>
      <h1>Home</h1>
      <p>Welcome to the home page!</p>
    </div>
  );
};

export default Home;
