import React, { useEffect } from 'react';
import axiosInstance from '../../utilis/axiosInstance';

const About = () => {
  useEffect(() => {
    // Example API call
    axiosInstance.get('https://api.example.com/data')
      .then(response => {
        console.log(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  }, []);

  return (
    <div>
      <h1>About</h1>
      <p>Welcome to the About page!</p>
    </div>
  );
};

export default About;
