import axios from 'axios';

const axiosInstance = axios.create();

axiosInstance.interceptors.request.use(
  config => {
    // Add any custom logic to modify the request config
    // For example, you can add headers or tokens here
    return config;
  },
  error => {
    // Handle request errors
    return Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  response => {
    // Handle successful responses
    return response;
  },
  error => {
    // Handle error responses
    return Promise.reject(error);
  }
);

export default axiosInstance;
